extends KinematicBody2D

export var speed = 500
export var danio = 10

var velocidad : Vector2

func start(pos, dir):
	rotation = dir
	position = pos
	velocidad = Vector2(speed, 0).rotated(rotation)

func _physics_process(delta):
	var collision = move_and_collide(velocidad * delta)
	if collision:
		queue_free()
		velocidad = velocidad.bounce(collision.normal)
		
		if collision.collider.has_method("hit"):
			get_tree().get_nodes_in_group("nivel")[0]._score_update()
			get_tree().get_nodes_in_group("nivel")[0].score += 10
			collision.collider.hit()



