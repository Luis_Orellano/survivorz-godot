extends KinematicBody2D

var Bullet = preload("res://Escenas/bullet.tscn")
var velocidad = 100
var vel_rot : float = 100
var puedo_disparar = false

var vel_lineal = Vector2()
#var can_shoot = true
var puedo_rotar = true
var velocidad_ = Vector2(0,0)
var speed = 150 #velocidad del bullet
var danio = 10 #daño bullet

export var vida_max = 200
export var vida_actual = 200
export var fire_rate = .5
var barravida
export var cant_bullet = 7


func _ready():
	barravida = get_tree().get_nodes_in_group("hp")[0]
	puedo_disparar = true
	
	$guns_timer.wait_time = fire_rate

func _physics_process(delta):
	control(delta)
	move_and_slide(vel_lineal)
	update_hp()
	#controlador.update_hp()
	#vida_actual += 1 * delta
	#vida_actual = clamp(vida_actual, 0, vida_max)

func control(delta):
	var Htarget = 0 #para moverse horizontalmente
	var Vtarget = 0 #para moverse verticalmente
	
	if Input.is_action_pressed("ui_left"):
		Htarget -= 1
	if Input.is_action_pressed("ui_right"):
		Htarget += 1
	if Input.is_action_pressed("ui_up"):
		Vtarget -= 1
	if Input.is_action_pressed("ui_down"):
		Vtarget += 1
	if Input.is_action_pressed("accion"):
		if puedo_disparar and cant_bullet>0 and cant_bullet<=7:
			$disparo.play()
			shoot()
	if Input.is_action_just_pressed("recargar"):
		reload()
	
	Htarget *= velocidad
	Vtarget *= velocidad
	vel_lineal = Vector2(Htarget, Vtarget)
	
	if puedo_rotar:
		var dir = (get_global_mouse_position() - global_position).normalized() #obtener el resultado de la posicion del mouse
		var actual_rot = Vector2(1,0).rotated(global_rotation)
		global_rotation = actual_rot.linear_interpolate(dir, vel_rot * delta).angle()

func shoot():
	var b = Bullet.instance()
	b.start($posicion.global_position, rotation)
	get_parent().add_child(b)
	
	b.global_position = $posicion.global_position
	puedo_disparar = false
	$guns_timer.start()
	get_tree().get_nodes_in_group("nivel")[0].cant_balas-=1
	cant_bullet -= 1

func update_hp():
	if (vida_actual == 0):
		game_over()
	else:
		barravida.value = vida_actual * barravida.max_value / vida_max

func game_over():
	barravida.value = 0
	get_tree().get_nodes_in_group("gameover")[0].gameover()
	get_tree().paused=true

func _on_guns_timer_timeout():
	puedo_disparar = true

func reload():
	if cant_bullet == 0:
		$recargar.play()
		get_tree().get_nodes_in_group("nivel")[0].cant_balas=7
		cant_bullet = 7
	else:
		$recargar.play()
		get_tree().get_nodes_in_group("nivel")[0].cant_balas=7
		cant_bullet = 7

func _on_Agua_body_entered(body):
	if body.name == "Personaje":
		var o = Color(1,1,1,.70)
		$Sprite.set_modulate(o)
		velocidad = 50

func _on_Agua_body_exited(body):
	if body.name == "Personaje":
		var o = Color(1,1,1,1)
		$Sprite.set_modulate(o)
		velocidad = 100
