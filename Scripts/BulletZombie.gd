extends Area2D

var speed = 400
var velocity = Vector2()

func start(pos, dir):
	position = pos
	rotation = dir
	velocity = Vector2(speed, 0).rotated(dir)

func _process(delta):
	position += velocity * delta

func _on_Bullet_body_entered(body):
	queue_free()

#func _on_VisibilityNotifier2D_screen_exited():
#	queue_free()

func _on_BulletZombie_body_entered(body):
	if body.name == "Personaje":
		get_tree().get_nodes_in_group("personaje")[0].vida_actual -= 10
		queue_free()
	else:
		queue_free()
