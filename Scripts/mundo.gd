extends Node

var cont_portal = 0
var max_portal = 5
var score = 0
var cant_balas = 7

onready var portal_n = load("res://Escenas/portal_sig_nivel.tscn")

func _ready():
	get_tree().paused=false
	var spawn = $StartPosition
	$GUI/BestoScore.text = "Best Score: "+ str(get_node("/root/controlador").newScore)
	get_tree().get_nodes_in_group("score")[0].text = "SCORE: "+ String(0) 
	get_tree().get_nodes_in_group("lblBalas")[0].text ="Balas:" +String(cant_balas)+"/7"

func _physics_process(delta):
	get_tree().get_nodes_in_group("lblBalas")[0].text = "Balas:  " +String(cant_balas)+"/7"
	get_tree().get_nodes_in_group("score")[0].text = "SCORE: "+ String(score) 

func new_game():
	$Personaje.start($StartPosition.position)

func _on_recargar_pressed():
	get_tree().reload_current_scene()

func _score_update():
	var best_score= get_node("/root/controlador").newScore
	if(score > best_score):
		get_node("/root/controlador").newScore=score+20
		get_node("/root/controlador").guardar_puntos()
	score += 10
	get_tree().get_nodes_in_group("score")[0].text = "SCORE: "+ String(score) 
 
#prueba

func _on_Spawn_portal_muerte():
	cont_portal += 1
	next_level()

func next_level():
	if cont_portal == max_portal:
		var newPortal = portal_n.instance()
		get_tree().get_nodes_in_group("nivel")[0].add_child(newPortal)
		newPortal.global_position = Vector2(835,367)
	else:
		pass
