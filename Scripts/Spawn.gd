extends Position2D

onready var enemigo = load("res://Escenas/Enemigo.tscn")

signal portal_muerte

#vida del portal
export var vida_max=100
export var vida_actual = 20
var barravida
var spawn
var cont_portal := 0
var max_portal := 5
#var max_enemi = 0
#var cont_enemi = 0

func _ready():
	pass

func _physics_process(delta): 
	update_hp()

func _on_Timer_timeout():
	var newEnemigo = enemigo.instance()
	get_tree().get_nodes_in_group("nivel")[0].add_child(newEnemigo)
	newEnemigo.global_position = global_position

func update_hp():
	barravida = get_node("ProgressBar")
	if (barravida != null):
		if vida_actual == 0:
			#get_tree().get_nodes_in_group("nivel")[0].cont_portal+=1
			queue_free()
			emit_signal("portal_muerte")
			get_tree().get_nodes_in_group("nivel")[0].score += 50
		else:
			barravida.value = vida_actual * barravida.max_value / vida_max
	else:
		barravida = Vector2(0,0)

func _on_Area2D_body_entered(body):
	if body.name == "Bullet":
		$ProgressBar.visible = true
		get_tree().get_nodes_in_group("bullet")[0].queue_free()
		vida_actual -= 10
	else: 
		pass

