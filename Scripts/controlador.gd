extends Node

var newScore = 0

func _ready():
	cargar_puntos()

func guardar_puntos():
	var save = File.new()
	save.open("res://scores.sav", File.WRITE)
	save.store_var(newScore)
	
	save.close()

func cargar_puntos():
	var cargar = File.new()
	if(!cargar.file_exists("res://scores.sav")):
		print("No existen scores nuevos")
		return
	else:
		cargar.open("res://scores.sav", File.READ)
		newScore = cargar.get_var()
		cargar.close()