extends KinematicBody2D

var cantidad = 0
export (int, 2, 10) var danio = 2
var movedir = Vector2(0,0)
var movetimer = 0
var movetimer_length = 15
var vel_enemigo = 100

var velocidad = Vector2()

var hitstun= 0

#Variables para ray-casting
export (int) var detect_radius
export (float) var fire_rate
export (PackedScene) var Bullet
#var vis_color = Color(.867,.91,.247,0.1)

var can_shoot = true
var target 
var hit_pos

func _ready():
	movedir = movimiento_enemigo()
	#$Sprite.self_modulate = Color(0.2,0,0)
	var shape = CircleShape2D.new()
	shape.radius = detect_radius
	$Visibility/CollisionShape2D.shape = shape
	$guns_timer.wait_time = fire_rate

func _process(delta):
	
	var motion=move_and_slide(velocidad * delta)
	movement(motion)
	
	actualizacion_mov()

func actualizacion_mov():
	if movetimer > 0:
		movetimer-=1
	if movetimer == 0 || is_on_wall():
		movedir = movimiento_enemigo()
		movetimer = movetimer_length
	
	if target:
		rotation = (target.position - position).angle()
		if can_shoot:
			aim()

func aim():
	var space_state = get_world_2d().direct_space_state
	var result = space_state.intersect_ray(position, target.position, [self], collision_mask)
	if result:
		hit_pos = result.position
		if result.collider.name == "Personaje":
			$Sprite.self_modulate.r = 1.0
			rotation = (target.position - position).angle()
			if can_shoot:
				shoot(target.position)

func shoot(pos):
	var b = Bullet.instance()
	var a = (pos - global_position).angle()
	b.start(global_position, a + rand_range(-0.05, 0.05))
	
	get_parent().add_child(b)
	b.global_position = $posicion.global_position
	can_shoot = false
	$guns_timer.start()

func movement(motion):
	motion = movedir.normalized() * vel_enemigo
	move_and_slide(motion)

func movimiento_enemigo():
	var new_direction = randi() % 4+1
	match new_direction:
		1:
			return Vector2.LEFT
		2:
			return Vector2.RIGHT
		3:
			return Vector2.UP
		4:
			return Vector2.DOWN

func _draw():
	pass#draw_circle(Vector2(), detect_radius, vis_color)

func hit():
	queue_free()

func _on_Area2D_body_entered(body):
	if body.name == "Personaje":
		target = body


func _on_Visibility_body_exited(body):
	if body==target:
		target=null


func _on_guns_timer_timeout():
	can_shoot = true
