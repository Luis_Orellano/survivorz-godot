extends Node2D

func _ready():
	pass

func _on_Button_pressed():
	get_tree().change_scene("res://Escenas/Mundo.tscn")

func _on_Exit_pressed():
	get_tree().quit()


func _on_Instruccion_pressed():
	if $Main/Como_jugar.visible == false:
		$Main/Como_jugar.visible = true
	else:
		$Main/Como_jugar.visible = false
